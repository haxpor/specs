Pod::Spec.new do |s|
    s.name      =   "tg-framework-ios"
    s.version   =   "1.0.0"
    s.summary   =   "SDK for TG"
    s.homepage  =   "https://github.com/playbasis/tg-framework-ios"
    s.license   =   "Copyrights"
    s.author    =   { "Playbasis" => "devteam@playbasis.com" }
    #s.source    =   { :git => "https://github.com/playbasis/tg-framework-ios.git", :tag => s.version }
    s.social_media_url = "https://twitter.com/playbasis"

    s.platform  =   :ios, '8.0'
    s.requires_arc  = true

    s.source    = { :http => "https://data.wasin.io/vendored_frameworks/1.0.0/TGSDK.zip" }
    s.ios.vendored_frameworks   =   "TGSDK-Release-iphoneuniversal/TGSDK.framework"
end